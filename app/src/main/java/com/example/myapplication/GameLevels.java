package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.content.Intent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class GameLevels extends AppCompatActivity {
    public final static String Level="level";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.game_lavels);
    }

    public void backToMain(View view) {
        Intent intent = new Intent(GameLevels.this, MainActivity.class);
        startActivity(intent);
    }

    public void chooseLevel(View view) {
        TextView tex = (TextView) findViewById(view.getId());
        Intent intent = new Intent(GameLevels.this, MainActivity.class);
        intent.putExtra(Level, tex.getText());
        startActivity(intent);
    }
}
