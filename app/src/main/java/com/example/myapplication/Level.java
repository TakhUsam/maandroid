package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class Level extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        // Получаем сообщение из объекта intent
        Intent intent = getIntent();
        String number = intent.getStringExtra(GameLevels.Level);

        TextView tv=findViewById(R.id.levelName);
        tv.setText(number);

        setContentView(R.layout.level);
    }

    public void backToLevels(View view) {
        Intent intent = new Intent(Level.this, MainActivity.class);
        startActivity(intent);
    }


}
